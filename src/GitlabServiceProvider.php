<?php

namespace RoobieBoobieee\Gitlab;

use Illuminate\Support\ServiceProvider;

class GitlabServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/Database/Migrations');
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        //
    }
}
