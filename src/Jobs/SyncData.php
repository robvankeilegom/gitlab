<?php

namespace RoobieBoobieee\Gitlab\Jobs;

use Illuminate\Bus\Queueable;
use RoobieBoobieee\Gitlab\Gitlab;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SyncData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    private $type;

    private $url;

    private $page;

    private $auto;

    private $username;

    private $password;

    private $extraData;

    /**
     * Create a new job instance.
     */
    public function __construct($username, $password, $type, $url, $extraData = [], $page = 1, $auto = true)
    {
        $this->username = $username;
        $this->password = $password;

        $this->type      = (new $type);
        $this->url       = $url;
        $this->page      = $page;
        $this->auto      = $auto;
        $this->extraData = $extraData;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $gl       = new Gitlab($this->username, $this->password);
        $data     = $gl->call($this->url, ['query' => ['page' => $this->page]]);
        $fillable = $this->type->getFillable();
        $pk       = $this->type->primaryKey();

        if (! $data) {
            \Log::error(\json_encode($data));

            return;
        }

        foreach ($data as $record) {
            $checksum = md5(json_encode($record));
            $obj      = $this->type::where($pk, $record->{$pk})->first();

            $record = $this->flattenObjectToArray($record);

            $record = array_filter($record, function ($key) use ($fillable) {
                return in_array($key, $fillable);
            }, ARRAY_FILTER_USE_KEY);

            foreach ($this->extraData as $key => $val) {
                if (! array_key_exists($key, $record)) {
                    $record[$key] = $val;
                }
            }

            if (! $obj || $obj->checksum !== $checksum) {
                if (! $obj) {
                    $obj = new $this->type((array) $record);

                    if ($gl->id() === $obj->owner_id) {
                        $obj->sync = true;
                    }
                } elseif ($obj->checksum !== $checksum) {
                    $obj->update((array) $record);
                }

                $obj->checksum = $checksum;

                if (method_exists($obj, 'setUserName')) {
                    $obj->setUserName($this->username);
                }
                $obj->save();
            }
        }

        if (method_exists($obj, 'saveLastPage')) {
            $obj->saveLastPage($this->page++);
        }

        if ($this->auto) {
            self::dispatch($this->username, $this->password, get_class($this->type), $this->url, $this->extraData, ++$this->page, $this->auto);
        }
    }

    public function flattenObjectToArray($object, $prefix = '')
    {
        $out = [];
        foreach ($object as $key => $value) {
            if ($prefix) {
                $key = implode('_', [$prefix, $key]);
            }

            if (is_object($value) || is_array($value)) {
                $value = $this->flattenObjectToArray($value, $key);
            }

            if ($key === 'created_at' || $key === 'last_activity_at' || $key === 'authored_date' || $key === 'committed_date') {
                $key   = 'gl_' . $key;
                $value = new \Carbon\Carbon($value);
            }

            if (is_array($value)) {
                $out = array_merge($out, $value);
            } else {
                $out[$key] = $value;
            }
        }

        return $out;
    }
}
